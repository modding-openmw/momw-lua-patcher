local core = require("openmw.core")
local MOD_ID = "MOMWLuaPatcher"
if core.API_REVISION < 59 then error(core.l10n(MOD_ID)("needNewerOpenMW")) end
local markup = require('openmw.markup')
local util = require('openmw.util')
local vfs = require('openmw.vfs')
local world = require("openmw.world")
local patchDir = "data"
local patched = {}
local scriptVersion = 1

-- Patch functions
local function disable(obj)
    obj.enabled = false
end

local function move(obj, data)
    obj:teleport(
        obj.cell,
        util.vector3(
            obj.position.x + data.xOffset,
            obj.position.y + data.yOffset,
            obj.position.z + data.zOffset
        )
    )
end

-- Action to function map
local actions = {
    ["disable"] = disable,
    ["move"] = move
}

-- Build a table of patches to apply at startup to avoid reading
-- files for every object active (which will happen all the time)
local patches = {}
for ymlFile in  vfs.pathsWithPrefix(patchDir) do
    local yml = markup.loadYaml(ymlFile)
    -- Skip bunk data/yml
    if yml == nil then
        print(string.format("[%s][WARNING] No or bad data in file: %s", MOD_ID, ymlFile))
        goto continue
    end
    for _, patch in pairs(yml) do
        table.insert(patches, patch)
    end
    ::continue::
end

-- Do stuff
local function onObjectActive(obj)
    if patched[obj.id] ~= nil then return end
    -- Do the patches
    for _, patch in pairs(patches) do
        local patchObj
        if patch.contentFile and patch.refNum then
            patchObj = world.getObjectByFormId(core.getFormId(patch.contentFile, patch.refNum))
        end
        if obj == patchObj and obj.contentFile == string.lower(patch.contentFile) then
            local func = actions[patch.action]
            if func then
                print(string.format("[%s][%s][%s]: %s", MOD_ID,
                                    patch.contentFile, patch.action, obj))
                func(obj, patch)
                patched[obj.id] = patch.action
            else
                error(string.format("ERROR: invalid action %s", patch.action))
            end
        end
    end
end

local function onLoad(data)
    if not data then return end
    patched = data.patched
end

local function onSave()
    return {
        patched = patched,
        scriptVersion = scriptVersion
    }
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave,
        onObjectActive = onObjectActive
    }
}
