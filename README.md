# MOMW Lua Patcher

A Lua-powered patcher by the MOMW community

#### Credits

Author: **johnnyhostile**

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/momw-lua-patcher/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\momw-lua-patcher

        # Linux
        /home/username/games/OpenMWMods/momw-lua-patcher

        # macOS
        /Users/username/games/OpenMWMods/momw-lua-patcher

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\momw-lua-patcher"`)
1. Add `content=momw-lua-patcher.omwaddon` and `content=momw-lua-patcher.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/momw-lua-patcher/-/issues)
* Email `momw-lua-patcher@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
